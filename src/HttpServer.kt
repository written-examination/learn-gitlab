import java.io.InputStream
import java.net.InetAddress
import java.net.ServerSocket

private const val add = "add"
private const val mult = "mult"

object HttpServer {
  fun run(host: String, port: Int) {
    val serverSocket = ServerSocket(port, 1, InetAddress.getByName(host))
    while (true) {
      val socket = serverSocket.accept()
      val inputStream = socket.getInputStream()
      val outputStream = socket.getOutputStream()
      val request = inputStream.toRequest()

      outputStream.write("""
        HTTP/1.1 200 OK
        
        <html>
          <head>
            <title>Server</title>
          </head>
          <body>
          ${
            when (request.method) {
              add -> request.param["a"]!!.toInt() + request.param["b"]!!.toInt()
              mult -> request.param["a"]!!.toInt() * request.param["b"]!!.toInt()
              else -> "暂不支持该请求"
            }
          }
          </body>
        </html>
      """.trimIndent().toByteArray())
      socket.close()
    }
  }
}

private class Request(
  val method: String,
  val param: Map<String, String>
)

private const val size = 2048
private val regex = "/(.*)\\u003F(.*)".toRegex()
private fun InputStream.toRequest(): Request {
  val res = StringBuilder(size)
  val buffer = ByteArray(size)
  val length = read(buffer)

  for(i in 0 until length)
    res.append(buffer[i].toChar())

  val path = res.toString().lines()[0].split(' ')[1]
  val values = regex.matchEntire(path)?.groupValues

  var method = ""
  val map = HashMap<String, String>()
  values?.let {
    method = it[1]

    val param = it[2].split("&")
    val p1 = param[0].split("=")
    val p2 = param[1].split("=")
    map[p1[0]] = p1[1]
    map[p2[0]] = p2[1]
  }
  return Request(method, map)
}